package com.apps.chatcripto;

public interface CriptoPresent {
    void decrypt(String cipherText, String type, String mode,int i);
    void encrypt(String plainText, String type, String mode);

}
