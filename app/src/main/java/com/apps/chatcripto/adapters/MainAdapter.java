package com.apps.chatcripto.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.chatcripto.MainActivity;
import com.apps.chatcripto.R;
import com.apps.chatcripto.models.Message;
import com.apps.chatcripto.utils.SCUtils;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MyViewHolder> {
  private ArrayList<Message> data;
  private Context mContext;

  public MainAdapter(Context context, ArrayList<Message> data) {
    this.data = data;
    this.mContext = context;
  }


    @Override
  public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
    View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_main, null);

    MyViewHolder viewHolder = new MyViewHolder(view);
    return viewHolder;
  }

  @Override
  public void onBindViewHolder(final MyViewHolder myViewHolder, int i) {
    final Message message = data.get(i);
    final String formatted_date = SCUtils.formatted_date(message.getTimestamp());
      myViewHolder.ui_change.setOnClickListener(new View.OnClickListener() {
      @RequiresApi(api = Build.VERSION_CODES.O)
      @Override
      public void onClick(View v) {
        if(myViewHolder.ui_change.getText()!="Plaintext"){
        try {
            myViewHolder.ui_change.setText("Plaintext");
            myViewHolder.textView_message.setText(
                    Html.fromHtml("<font color=\"#403835\">&#x3C;" + message.getUsername() + "&#x3E;</font>" + " " + decrypt(message.getMessage(),MainActivity.type,"OFB") + " is "+message.getCripto()+ " <font color=\"#999999\">" + formatted_date + "</font>"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        }else{
            myViewHolder.ui_change.setText("Change");
            myViewHolder.textView_message.setText(
                    Html.fromHtml("<font color=\"#403835\">&#x3C;" + message.getUsername() + "&#x3E;</font>" + " " + message.getMessage() + " is "+message.getCripto()+ " <font color=\"#999999\">" + formatted_date + "</font>"));

        }
      }
    });
    if (message.isNotification()) {
      myViewHolder.textView_message.setText(Html.fromHtml("<small><i><font color=\"#FFBB33\">" + " " + message.getMessage() + "</font></i></small>"));
    }else {
      myViewHolder.textView_message.setText(
          Html.fromHtml("<font color=\"#403835\">&#x3C;" + message.getUsername() + "&#x3E;</font>" + " " + message.getMessage() + " is "+message.getCripto() + " <font color=\"#999999\">" + formatted_date + "</font>"));
    }
  }

  @Override
  public int getItemCount() {
    return (null != data ? data.size() : 0);
  }

    public class MyViewHolder extends RecyclerView.ViewHolder {
    protected TextView textView_message;
    protected Button ui_change;

    public MyViewHolder(View view) {
      super(view);
      this.textView_message = (TextView) view.findViewById(R.id.textView_message);
      this.ui_change = (Button) view.findViewById(R.id.ui_change);

    }
  }
    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.O)
    public String decrypt(String cipherText, String type, String mode) throws Exception {
        try {
            Cipher cipher = Cipher.getInstance(type+"/"+mode+"/PKCS5Padding");
            SecretKeySpec key = new SecretKeySpec(MainActivity.key_to_bytes, type);
            cipher.init(Cipher.DECRYPT_MODE, key,new IvParameterSpec(MainActivity.iv_to_bytes));
            int flags = Base64.NO_WRAP | Base64.URL_SAFE;
            byte[] decryptedText = cipher.doFinal(Base64.decode(cipherText.getBytes(),flags));

            return new String(decryptedText);
        } catch (InvalidAlgorithmParameterException ex){
            Toast.makeText(mContext, "Gagal", Toast.LENGTH_LONG).show();
        } catch (NoSuchAlgorithmException ex) {
            Toast.makeText(mContext, "Gagal", Toast.LENGTH_LONG).show();
        } catch (IllegalBlockSizeException ex) {
            Toast.makeText(mContext, "Gagal", Toast.LENGTH_LONG).show();
        } catch (BadPaddingException ex) {
            Toast.makeText(mContext, "Gagal", Toast.LENGTH_LONG).show();
        } catch (InvalidKeyException ex) {
            Toast.makeText(mContext, "Gagal", Toast.LENGTH_LONG).show();
        } catch (NoSuchPaddingException ex) {
        }
        return "gagal";
    }
}