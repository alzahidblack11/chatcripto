package com.apps.chatcripto.utils;

public interface RandomGenerator {

    /**
     * Returns N random bits
     *
     * See also java.util.Random#next
     * @param numBits
     * @return and int with the LSB being random
     */
    public int next(int numBits);

}
