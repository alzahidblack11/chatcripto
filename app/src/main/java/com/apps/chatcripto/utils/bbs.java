package com.apps.chatcripto.utils;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

public class bbs implements RandomGenerator {

    private static final BigInteger two = BigInteger.valueOf(2L);

    private static final BigInteger three = BigInteger.valueOf(3L);

    private static final BigInteger four = BigInteger.valueOf(4L);

    private BigInteger n;

    private BigInteger state;

    public static BigInteger generateN() {
        BigInteger p =  new BigInteger("13231090003");
        BigInteger q =  new BigInteger("1853105600379");

        return p.multiply(q);
    }

    public bbs(int bits) {
        this(bits, new Random());
    }

    public bbs(int bits, Random rand) {
        this(generateN());
    }

    public bbs(BigInteger n) {
        this(n, SecureRandom.getSeed(n.bitLength() / 8));
    }

    public bbs(BigInteger n, byte[] seed) {
        this.n = n;
        setSeed(seed);
    }

    public void setSeed(byte[] seedBytes) {
        // ADD: use hardwired default for n
        BigInteger seed = new BigInteger(1, seedBytes);
        state = seed.mod(n);
    }

    public int next(int numBits) {
        // TODO: find out how many LSB one can extract per cycle.
        //   it is more than one.
        int result = 0;
        for (int i = numBits; i != 0; --i) {
            state = state.modPow(two, n);
            result = (result << 1) | (state.testBit(0) == true ? 1 : 0);
        }
        return result;
    }
}