package com.apps.chatcripto;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Base64;
import android.widget.Toast;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import static com.apps.chatcripto.MainActivity.iv_to_bytes;
import static com.apps.chatcripto.MainActivity.key_to_bytes;

public class CriptoPresentImp implements CriptoPresent {
    private CriptoView criptoView;

    public CriptoPresentImp(CriptoView criptoView) {
        this.criptoView = criptoView;
    }


    @Override
    public void decrypt(String cipherText, String type, String mode, int i) {
        try {
            Cipher cipher = Cipher.getInstance(type+"/"+mode+"/PKCS5Padding");
            SecretKeySpec key = new SecretKeySpec(key_to_bytes, type);
            cipher.init(Cipher.DECRYPT_MODE, key,new IvParameterSpec(iv_to_bytes));
            int flags = Base64.NO_WRAP | Base64.URL_SAFE;
            byte[] decryptedText = cipher.doFinal(Base64.decode(cipherText.getBytes(),flags));
            criptoView.decryptSuccess(new String(decryptedText), i);
        } catch (InvalidAlgorithmParameterException ex){
            criptoView.decryptError();
        } catch (NoSuchAlgorithmException ex) {
            criptoView.decryptError();
        } catch (IllegalBlockSizeException ex) {
            criptoView.decryptError();
        } catch (BadPaddingException ex) {
            criptoView.decryptError();
        } catch (InvalidKeyException ex) {
            criptoView.decryptError();
        } catch (NoSuchPaddingException ex) {
            criptoView.decryptError();
        } catch (IllegalArgumentException ex){
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.FROYO)
    @Override
    public void encrypt(String plainText, String type, String mode) {
        try {
            byte[] clean = plainText.getBytes();
            Cipher cipher = Cipher.getInstance(type+"/"+mode+"/PKCS5Padding");
            SecretKeySpec seckey = new SecretKeySpec(key_to_bytes, type);
            cipher.init(Cipher.ENCRYPT_MODE, seckey,new IvParameterSpec(iv_to_bytes));
            int flags = Base64.NO_WRAP | Base64.URL_SAFE;
            criptoView.encryptSuccess(Base64.encodeToString(cipher.doFinal(clean),flags));
        } catch (NoSuchAlgorithmException ex) {
            criptoView.encryptError();
        } catch (InvalidAlgorithmParameterException ex){
            criptoView.encryptError();
        } catch (IllegalBlockSizeException ex) {
            criptoView.encryptError();
        } catch (BadPaddingException ex) {
            criptoView.encryptError();
        } catch (InvalidKeyException ex) {
            criptoView.encryptError();
        } catch (NoSuchPaddingException e) {
            criptoView.encryptError();
        }
    }
}
