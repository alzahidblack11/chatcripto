package com.apps.chatcripto;

public interface CriptoView {
    void encryptSuccess(String text);
    void decryptSuccess(String text,int i);
    void encryptError();
    void decryptError();
}

